const keys = require('../keys/index');

module.exports = email => {
    return {
        from: 'Courses App <managsystem07@gmail.com>',
        to: email, 
        subject: 'Congratulations! You are successfully registred on out site',
        html: `
            <h1>Wellcome to Courses Application</h1>
            <p>You have successfully created your account with email - ${email}</p>
            <hr />
            <a href="${keys.BASE_URL}">Courses App</a>
        `
    }
}