const keys = require('../keys/index');

module.exports = (email, token) => {
    return {
        from: 'Courses App <managsystem07@gmail.com>',
        to: email, 
        subject: 'Access recovery',
        html: `
            <h1>Do you forgot password?</h1>
            <p>If not, please ignore this letter.</p>
            <p>Otherwise click the link: </p>
            <p><a href="${keys.BASE_URL}/auth/password/${token}">Access recovery</a></p>
            <hr />
            <a href="${keys.BASE_URL}">Courses App</a>
        `
    }
}