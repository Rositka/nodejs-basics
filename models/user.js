const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: String,
    password: {
        type: String,
        required: true
    },
    avatarUrl: String,
    resetToken: String,
    resetTokenExp: Date,
    card: {
        items: [
            {
                count: {
                    type: Number,
                    required: true,
                    default: 1
                },
                courseId: {
                    type: Schema.Types.ObjectId,
                    ref: 'Course',
                    required: true
                }
            }
        ]
    }
});

userSchema.methods.addToCard = function(course) {
    const clonedItems = [...this.card.items];
    const idx = clonedItems.findIndex(c => c.courseId.toString() === course._id.toString());

    if (idx >= 0) {
        clonedItems[idx].count++
    } else {
        clonedItems.push({
            courseId: course._id,
            count: 1
        });
    }

    this.card = {items: clonedItems};

    return this.save();
}

userSchema.methods.removeFromCard = function(id) {
    let clonedItems = [...this.card.items];
    const idx = clonedItems.findIndex(c => c.courseId.toString() === id.toString());

    if (clonedItems[idx].count === 1) {
        clonedItems = clonedItems.filter(c => c.courseId.toString() !== id.toString());
    } else {
        clonedItems[idx].count--
    }

    this.card = {items: clonedItems};

    return this.save();
}

userSchema.methods.clearCard = function(id) {
    this.card = {items: []};

    return this.save();
}

module.exports = model('User', userSchema);